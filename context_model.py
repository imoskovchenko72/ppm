from typing import NamedTuple, Optional


class ContextModel(NamedTuple):
    context: bytes
    parent_model: Optional["ContextModel"]
    context_count: int
    next_symbols_count: dict[bytes, int]
