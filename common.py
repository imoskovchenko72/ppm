from collections import defaultdict
from typing import Optional

from context_model import ContextModel
from context_model_tree import ContextModelTree


def get_longest_context_model(context: bytes, context_models_tree: ContextModelTree) -> ContextModel:
    for i in range(len(context)):
        maybe_model = context_models_tree.maybe_get_model(context[i:])
        if maybe_model is not None:
            return maybe_model
    return context_models_tree.maybe_get_model(b"")


def _add_to_model(c: bytes, context_models_tree: ContextModelTree, model: ContextModel):
    if c not in model.next_symbols_count:
        model.next_symbols_count[c] = 1
        next_context = model.context + c
        next_model = ContextModel(
            next_context, context_models_tree.maybe_get_model(next_context[1:]), 1, defaultdict(int)
        )
        context_models_tree.add_model(next_context, next_model)
    else:
        model.next_symbols_count[c] += 1


####### Возможные способы кодирования #######
def update_context_tree_simple(context: bytes, c: bytes, context_models_tree: ContextModelTree):
    model = context_models_tree.maybe_get_model(b"")
    _add_to_model(c, context_models_tree, model)

    for i in range(len(context) - 1, -1, -1):
        model = context_models_tree.maybe_get_model(context[i:])
        assert model is not None
        _add_to_model(c, context_models_tree, model)


def update_context_tree_with_exceptions(context: bytes, c: bytes, context_models_tree: ContextModelTree):
    if context_models_tree.maybe_get_model(c) is None:
        next_context = c
        next_model = ContextModel(
            next_context, context_models_tree.maybe_get_model(b""), 1, defaultdict(int)
        )
        context_models_tree.add_model(next_context, next_model)

    for i in range(len(context) - 1, -1, -1):
        next_context = context[i:] + c
        if context_models_tree.maybe_get_model(next_context) is None:
            next_model = ContextModel(
                next_context, context_models_tree.maybe_get_model(next_context[1:]), 1, defaultdict(int)
            )
            context_models_tree.add_model(next_context, next_model)

    context_model = get_longest_context_model(context, context_models_tree)
    while c not in context_model.next_symbols_count:
        context_model = context_model.parent_model

    for i in range(len(context) - len(context_model.context)):
        maybe_model = context_models_tree.maybe_get_model(context[i:])
        assert maybe_model is not None

        if maybe_model:
            maybe_model.next_symbols_count[c] += 1

    context_model.next_symbols_count[c] += 1
    context_model = context_model.parent_model
    if context_model is None:
        return
    while len(context_model.context) > 0:
        if len(context_model.next_symbols_count) == 0 or (len(context_model.next_symbols_count) == 1 and c in context_model.next_symbols_count.keys()):
            context_model.next_symbols_count[c] += 1
        context_model = context_model.parent_model


####### Возможные способы подсчета вероятности ухода #######
def get_symbols_count_a(context_model: ContextModel, impossible_symbols: set[bytes]) -> dict[Optional[bytes], int]:
    """
    Реализует так называемый способ А определения вероятности ухода
    (считаем, что символ ухода всегда встречался ровно один раз)
    :param context_model: контекстная модель, содержащая статистику символов
    :param impossible_symbols: каких символов точно не может быть и их не стоит учитывать при кодировании
    :return: словарь (символ, сколько раз он мог встречаться)
    """
    count = {k: v for k, v in context_model.next_symbols_count.items() if k not in impossible_symbols}
    count.update({None: 1})
    return count


def get_symbols_count_b(context_model: ContextModel, impossible_symbols: set[bytes]) -> dict[Optional[bytes], int]:
    """
    Реализует так называемый способ B определения вероятности ухода
    (считаем, что когда символ встречается первый раз это на самом деле символ ухода)
    :param context_model: контекстная модель, содержащая статистику символов
    :param impossible_symbols: каких символов точно не может быть и их не стоит учитывать при кодировании
    :return: словарь (символ, сколько раз он мог встречаться)
    """
    count = {k: max(1, v - 1) for k, v in context_model.next_symbols_count.items() if k not in impossible_symbols}
    count.update({None: max(1, len(context_model.next_symbols_count))})
    return count


def get_symbols_count_c(context_model: ContextModel, impossible_symbols: set[bytes]) -> dict[Optional[bytes], int]:
    """
    Реализует так называемый способ B определения вероятности ухода
    (считаем, что символ ухода всегда столько раз, сколько уникальных символов)
    :param context_model: контекстная модель, содержащая статистику символов
    :param impossible_symbols: каких символов точно не может быть и их не стоит учитывать при кодировании
    :return: словарь (символ, сколько раз он мог встречаться)
    """
    count = {k: v for k, v in context_model.next_symbols_count.items() if k not in impossible_symbols}
    count.update({None: max(1, len(context_model.next_symbols_count))})
    return count


def get_symbols_count_d(context_model: ContextModel, impossible_symbols: set[bytes]) -> dict[Optional[bytes], int]:
    """
    Реализует так называемый способ B определения вероятности ухода
    (считаем, что когда символ встречается первый раз это на самом деле половина символа ухода и половина оригинального)
    :param context_model: контекстная модель, содержащая статистику символов
    :param impossible_symbols: каких символов точно не может быть и их не стоит учитывать при кодировании
    :return: словарь (символ, сколько раз он мог встречаться)
    """
    count = {k: v * 2 - 1 for k, v in context_model.next_symbols_count.items() if k not in impossible_symbols}
    count.update({None: max(1, len(context_model.next_symbols_count))})
    return count

####### Константы #######


MAX_CONTEXT_LENGTH = 5
ARITHMETIC_CODING_WINDOW_SIZE = 32

update_context_tree = update_context_tree_simple
get_symbols_count = get_symbols_count_a
