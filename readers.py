from typing import Optional


class InputBitStream:
    def __init__(self, input_filepath: str):
        self._input_filepath = input_filepath
        self._buffer = []
        self._start_buffer_pos = 0
        self._buffer_max_size = 4096

    def read_number(self, start: int, bit_len: int) -> int:
        useless_bits_count = start % 8
        mask = 0
        mask_len = 0

        pos = start // 8
        while mask_len < bit_len + useless_bits_count:
            c = self.get_byte(pos)
            mask_len += 8
            mask = (mask << 8) | (c or 0)
            pos += 1

        mask &= ((1 << (mask_len - useless_bits_count)) - 1)
        mask_len -= useless_bits_count
        return mask >> (mask_len - bit_len)

    def get_byte(self, pos: int) -> Optional[int]:
        if (pos > self._start_buffer_pos + self._buffer_max_size - 1 or
                pos < self._start_buffer_pos or len(self._buffer) == 0):
            self._buffer = []
            with open(self._input_filepath, "rb") as f:
                self._start_buffer_pos = max(0, pos - 20)
                f.seek(self._start_buffer_pos)
                self._buffer = list(f.read(self._buffer_max_size))
                while len(self._buffer) < self._buffer_max_size:
                    self._buffer.append(0)
        return self._buffer[pos - self._start_buffer_pos]


class FileReader:
    def __init__(self, input_filepath: str):
        self._input_filepath = input_filepath

    def __iter__(self):
        with open(self._input_filepath, "rb") as f:
            while True:
                c = f.read(1)
                if not c:
                    break
                yield c
        yield b"\0"
