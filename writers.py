class OutputBitStream:
    """
    Нельзя в файл записывать по одному биту, поэтому надо уметь их собирать в блоки хотя бы по 8 бит
    """
    def __init__(self, output_filepath: str):
        self._output_filepath = output_filepath

        self._bytes_buffer = []
        self._tail_mask = 0  # last < 8 bits
        self._tail_len = 0

    def write(self, bitmask: int, real_len: int):
        self._tail_len += real_len
        self._tail_mask = (self._tail_mask << real_len) | bitmask
        while self._tail_len >= 8:
            byte = self._tail_mask >> (self._tail_len - 8)

            self._bytes_buffer.append(byte.to_bytes())
            if len(self._bytes_buffer) > 1024:
                # не копим в оперативной памяти много, пишем лишнее на диск
                self._flush_buffer()

            self._tail_mask &= ((1 << (self._tail_len - 8)) - 1)
            self._tail_len -= 8

    def flush(self):
        if self._tail_len > 0:
            self._tail_mask <<= 8 - self._tail_len
            self._bytes_buffer.append(self._tail_mask.to_bytes())
            self._tail_mask = 0
            self._tail_len = 0
        self._flush_buffer()

    def _flush_buffer(self):
        with open(self._output_filepath, "ab") as f:
            for byte in self._bytes_buffer:
                f.write(byte)
        self._bytes_buffer = []


class FileWriter:
    def __init__(self, output_filepath: str):
        self._output_path = output_filepath
        self._buffer = []

    def write(self, c: bytes):
        self._buffer.append(c)
        if len(self._buffer) > 1024:
            self._flush_buffer()

    def flush(self):
        self._flush_buffer()

    def _flush_buffer(self):
        with open(self._output_path, "ab") as f:
            for c in self._buffer:
                f.write(c)
            self._buffer = []
