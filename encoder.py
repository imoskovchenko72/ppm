import os
import sys

import common
from arithmetic_encoding import ArithmeticEncoder
from context_model_tree import ContextModelTree
from readers import FileReader
from writers import OutputBitStream


def encode(input_filepath: str, output_filepath: str):
    bit_stream = OutputBitStream(output_filepath)
    context = b""
    contexts_tree = ContextModelTree()
    encoder = ArithmeticEncoder()

    file_size = os.path.getsize(input_filepath)
    bit_stream.write(file_size, 64)

    for c in FileReader(input_filepath):
        context_model = common.get_longest_context_model(context, contexts_tree)
        impossible_symbols = set()

        while c not in context_model.next_symbols_count:
            # кодируем символы ухода в моделях, не содержащих символ c
            # print(context_model.context, context_model.next_symbols_count)
            symbols_count = common.get_symbols_count(context_model, impossible_symbols)
            bitmask, real_len = encoder.encode_next_symbol(symbols_count, None)
            bit_stream.write(bitmask, real_len)
            impossible_symbols = impossible_symbols | set(context_model.next_symbols_count.keys())

            context_model = context_model.parent_model

        symbols_count = common.get_symbols_count(context_model, impossible_symbols)
        bitmask, real_len = encoder.encode_next_symbol(symbols_count, c)
        bit_stream.write(bitmask, real_len)

        common.update_context_tree(context, c, contexts_tree)
        context += c
        if len(context) > common.MAX_CONTEXT_LENGTH:
            context = context[1:]

    bit_stream.flush()


if __name__ == "__main__":
    # input_filepath = "data/good_test.txt"
    # output_filepath = "data/da.txt"
    input_filepath = sys.argv[1]
    output_filepath = sys.argv[2]

    if os.path.isfile(output_filepath):
        print("output file already exists")
        exit(0)

    encode(input_filepath, output_filepath)
