from collections import defaultdict
from typing import Optional

from context_model import ContextModel


class ContextModelTree:
    def __init__(self):
        default_model = ContextModel(b"", None, 0, {i.to_bytes(): self._get_default_frequency(i) for i in range(256)})
        root = ContextModel(b"", default_model, 0, defaultdict(int))
        self._models = {b"": root}

    def maybe_get_model(self, context: bytes) -> Optional[ContextModel]:
        return self._models.get(context)

    def add_model(self, context: bytes, model: ContextModel):
        self._models[context] = model

    @staticmethod
    def _get_default_frequency(i: int):
        if chr(i) == " ":
            return 250
        if ord("a") <= i <= ord("z"):
            return 100
        if ord("A") <= i <= ord("Z"):
            return 80
        if chr(i) in [".", ",", "?", "!", ":", "-", ";"]:
            return 20
        if chr(i) in ["`", "'", "\"", "(", ")", "+", "="]:
            return 12
        if ord("0") <= i <= ord("9"):
            return 10

        return 1
