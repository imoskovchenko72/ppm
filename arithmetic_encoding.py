from typing import Optional

import common
from readers import InputBitStream


INTERVAL_LENGTH = 1 << common.ARITHMETIC_CODING_WINDOW_SIZE


def ceil_div(a: int, b: int) -> int:
    return (a + b - 1) // b


def get_left_border(symbols_count: dict[Optional[bytes], int], c: Optional[bytes], l: int, r: int) -> int:
    if c is None:
        frequency_sum = sum(symbols_count.values()) - symbols_count.get(None, 0)
    else:
        frequency_sum = 0
        for key in sorted(key for key in symbols_count.keys() if key is not None):
            if key < c:
                frequency_sum += symbols_count[key]

    total = sum(symbols_count.values())
    a = ceil_div(frequency_sum * INTERVAL_LENGTH, total)
    return l + ceil_div(a * (r - l + 1), INTERVAL_LENGTH)


def get_right_border(symbols_count: dict[Optional[bytes], int], c: Optional[bytes], l: int, r: int) -> int:
    if c is None:
        frequency_sum = sum(symbols_count.values())
    else:
        frequency_sum = 0
        for key in sorted(key for key in symbols_count.keys() if key is not None):
            if key <= c:
                frequency_sum += symbols_count[key]

    total = sum(symbols_count.values())
    b = ceil_div(frequency_sum * INTERVAL_LENGTH, total) - 1

    return l + ceil_div((b + 1) * (r - l + 1), INTERVAL_LENGTH) - 1


def get_common_bit_prefix(a: int, b: int) -> [int, int]:
    ans = 0
    len = 0
    for i in range(common.ARITHMETIC_CODING_WINDOW_SIZE - 1, -1, -1):
        if (a & (1 << i)) == (b & (1 << i)):
           ans = ans * 2 + ((a >> i) & 1)
           len += 1
        else:
            break
    return ans, len


def shift_window(a: int, shift_size: int, bit: int) -> int:
    for _ in range(shift_size):
        a = (a * 2 + bit) & (INTERVAL_LENGTH - 1)
    return a


class ArithmeticEncoder:
    def __init__(self):
        self.l = 0
        self.r = INTERVAL_LENGTH - 1
        self.bits = 0

    def encode_next_symbol(self, symbols_count: dict[Optional[bytes], int], symbol: Optional[bytes]) -> [int, int]:
        """
        :param symbols_count:
        :param symbol:
        :return: Пара (битовая маска, ее настоящая длина с учетом ведущих нулей)
        """
        new_l = get_left_border(symbols_count, symbol, self.l, self.r)
        new_r = get_right_border(symbols_count, symbol, self.l, self.r)

        common_pref, common_pref_len = get_common_bit_prefix(new_l, new_r)

        new_l = shift_window(new_l, common_pref_len, 0)
        new_r = shift_window(new_r, common_pref_len, 1)

        if common_pref_len > 0 and self.bits > 0:
            bit = (common_pref >> (common_pref_len - 1)) & 1
            inverted_bit = bit ^ 1
            value = bit
            for _ in range(self.bits):
                value = (value * 2) | inverted_bit
            if common_pref_len > 1:
                value = (value << (common_pref_len - 1)) | (common_pref & ((1 << (common_pref_len - 1)) - 1))
            common_pref = value
            common_pref_len += self.bits
            self.bits = 0

        self.l, self.r = new_l, new_r

        new_bits_count = 0
        for i in range(common.ARITHMETIC_CODING_WINDOW_SIZE - 2, -1, -1):
            if ((self.l >> i) & 1) == 1 and ((self.r >> i) & 1) == 0:
                new_bits_count = common.ARITHMETIC_CODING_WINDOW_SIZE - 1 - i
            else:
                break

        if new_bits_count > 0:
            self.bits += new_bits_count
            for _ in range(new_bits_count):
                self.l = 2 * self.l - INTERVAL_LENGTH // 2
                self.r = 2 * self.r - INTERVAL_LENGTH // 2 + 1

        return common_pref, common_pref_len


class ArithmeticDecoder:
    def __init__(self, stream: InputBitStream):
        self.stream = stream
        self.l = 0
        self.r = INTERVAL_LENGTH - 1
        self.bits = 0
        self.pos = 64  # first 64 bits is file size

    def decode_next_symbol(self, symbols_count: dict[Optional[bytes], int]) -> Optional[bytes]:
        w = self.stream.read_number(self.pos, 1) << (common.ARITHMETIC_CODING_WINDOW_SIZE - 1)
        w |= self.stream.read_number(self.pos + 1 + self.bits, common.ARITHMETIC_CODING_WINDOW_SIZE - 1)
        symbol = None
        new_l = -1
        new_r = -1
        for c in symbols_count.keys():
            c_l = get_left_border(symbols_count, c, self.l, self.r)
            c_r = get_right_border(symbols_count, c, self.l, self.r)
            if c_l <= w <= c_r:
                symbol = c
                new_l = c_l
                new_r = c_r
                break

        common_pref, common_pref_len = get_common_bit_prefix(new_l, new_r)

        new_l = shift_window(new_l, common_pref_len, 0)
        new_r = shift_window(new_r, common_pref_len, 1)

        if common_pref_len > 0 and self.bits > 0:
            bit = (common_pref >> (common_pref_len - 1)) & 1
            inverted_bit = bit ^ 1
            value = bit
            for _ in range(self.bits):
                value = (value * 2) | inverted_bit
            if common_pref_len > 1:
                value = (value << (common_pref_len - 1)) | (common_pref & ((1 << (common_pref_len - 1)) - 1))
            common_pref = value
            common_pref_len += self.bits
            self.bits = 0

        self.l, self.r = new_l, new_r

        new_bits_count = 0
        for i in range(common.ARITHMETIC_CODING_WINDOW_SIZE - 2, -1, -1):
            if ((self.l >> i) & 1) == 1 and ((self.r >> i) & 1) == 0:
                new_bits_count = common.ARITHMETIC_CODING_WINDOW_SIZE - 1 - i
            else:
                break

        if new_bits_count > 0:
            self.bits += new_bits_count
            for _ in range(new_bits_count):
                self.l = 2 * self.l - INTERVAL_LENGTH // 2
                self.r = 2 * self.r - INTERVAL_LENGTH // 2 + 1

        self.pos += common_pref_len

        return symbol
