import os
import sys

import common
from arithmetic_encoding import ArithmeticDecoder
from context_model_tree import ContextModelTree
from readers import InputBitStream
from writers import FileWriter


def decode(input_filepath: str, output_filepath: str):
    bit_stream = InputBitStream(input_filepath)
    context = b""
    contexts_tree = ContextModelTree()
    decoder = ArithmeticDecoder(bit_stream)
    writer = FileWriter(output_filepath)

    file_size = bit_stream.read_number(0, 64)

    for _ in range(file_size):
        context_model = common.get_longest_context_model(context, contexts_tree)
        impossible_symbols = set()

        while True:
            symbols_count = common.get_symbols_count(context_model, impossible_symbols)
            c = decoder.decode_next_symbol(symbols_count)
            if c is None:
                impossible_symbols = impossible_symbols | set(context_model.next_symbols_count.keys())
                context_model = context_model.parent_model
            else:
                break

        common.update_context_tree(context, c, contexts_tree)
        context += c
        if len(context) > common.MAX_CONTEXT_LENGTH:
            context = context[1:]

        writer.write(c)
    writer.flush()


if __name__ == "__main__":
    # input_filepath = "data/t8.shakespeare.txt"
    # output_filepath = "data/da.txt"
    input_filepath = sys.argv[1]
    output_filepath = sys.argv[2]

    if os.path.isfile(output_filepath):
        print("output file already exists")
        exit(0)

    decode(input_filepath, output_filepath)
