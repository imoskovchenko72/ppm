# Запуск:

## кодирование
```shell
python3 encoder.py <intput file> <output file>
```

## декодирвоание
```shell
python3 decoder.py <input file> <output file>
```

# Настройки параметров
Все общие параметры, используемые как кодером, так и декодером, находятся в `common.py` в в разделе константы. 
Там можно поменять максимальную глубину контекстного дерева, способ оценки вероятности символа ухода, исключения при
обновлении дерева контекстных моделей. Файл может закодирован и раскодирован только с одинаковыми параметрами
